/* 
 * Copyright (c) 2018 - 2019 Qube 4D. All Rights Reserved
 *
 */

const {FusionAuthClient} = require("fusionauth-node-client");

/**
 * Initializes a new FusionAuthClient
 * 
 * @param {string} apiKey API key value belonging to tenant . 
 * @param {string} hostURL FusionAuth server URL.
 * @param {string} tenantId Client app's FusionAuth tenant id.
 * @return {FusionAuthClient}  A newly initialized FusionAuthClient object that can be used to query the FusionAuth server
 */

const initClient = function(apiKey, hostURL, tenantId){

    var client = new FusionAuthClient(apiKey, hostURL);
    if (tenantId){
        client.setTenantId(tenantId);
    }
    return client;
}

const HiveAuthClient = function(apiKey, hostURL, tenantId){
    this.client = initClient(apiKey, hostURL, tenantId);
};

HiveAuthClient.constructor = HiveAuthClient;

HiveAuthClient.prototype = {
    /**
     * User login
     * 
     * @param {string} userId User login Id
     * @param {string} pass User password
     * @param {string} applicationId Application's Id. User will be logged into this app.   
     * @return {Promise<ClientResponse<LoginResponse>>} A Promise for the FusionAuth call. It is to be fulfilled/rejected at top-level.
     */
    login: function(userId, pass, applicationId){
        var request = {
            loginId: userId,
            password: pass,
            applicationId: applicationId,
        };

        return this.client.login(request);
    },
    /**
    * User logout
    * 
    * @param {boolean} global When set to true, all refresh tokens issued to the user will be revoked.
    * @param {string} refreshToken 
    * @return {Promise<ClientResponse<void>>} A Promise for the FusionAuth call. It is to be fulfilled/rejected at top-level.
    */
    logout: function(global, refreshToken){
        return this.client.logout(global, refreshToken);
    },

    /**
    * Issue a new access token (JWT) for the requested application
    * 
    * @param {string} applicationId The Application Id for which you are requesting a new access token be issued.
    * @param {string} encodedJWT The encoded JWT (access token). 
    * @return {Promise<ClientResponse<void>>} A Promise for the FusionAuth call. It is to be fulfilled/rejected at top-level.
    */
    issueJWT: function(applicationId, encodedJWT){
        return this.client.issueJWT(applicationId, encodedJWT);
    },

    /**
    * Exchange a refresh token for a new JWT.
    *
    * @param {RefreshRequest} request The refresh request.
    * @return {Promise<ClientResponse<RefreshResponse>>} A Promise for the FusionAuth call. It is to be fulfilled/rejected at top-level.
    */
    refreshAccessToken: function(refreshToken){
        var request = {
            refreshToken: refreshToken
        };
        return this.client.exchangeRefreshTokenForJWT(request);
    },

    /**
    * Retrieves all refresh tokens for a user .
    *
    * @param {string} userId The Id of the user.
    * @return {Promise<ClientResponse<RefreshResponse>>} A Promise for the FusionAuth call. It is to be fulfilled/rejected at top-level.
    */
    retrieveUserRefreshTokens: function(userId){
        return this.client.retrieveRefreshTokens(userId);
    },

    /**
    * Revokes all tokens for a user. 
    *
    * @param {string} userId The user id whose tokens to delete.
    * @return {Promise<ClientResponse<void>>} A Promise for the FusionAuth call.
    */
    revokeUserRefreshTokens: function(userId){
        return this.client.revokeRefreshToken("", userId, "");
    },

    /**
    * Revokes or all tokens for an application. 
    *
    * @param {string} applicationId The application id of the tokens to delete.
    * @return {Promise<ClientResponse<void>>} A Promise for the FusionAuth call.
    */
    revokeAppRefreshTokens: function(applicationId){
        return this.client.revokeRefreshToken("", "", applicationId);
    },

    /**
    * Revokes all the refresh tokens for a user in an application.
    *
    * @param {string} userId (Optional) The user id whose tokens to delete.
    * @param {string} applicationId (Optional) The application id of the tokens to delete.
    * @return {Promise<ClientResponse<void>>} A Promise for the FusionAuth call.
    */
    revokeUserAppRefreshTokens: function(userId, applicationId){
        return this.client.revokeRefreshToken("", userId, applicationId);
    },

    /**
    * Revokes a single refresh token
    *
    * @param {string} token  The refresh token to delete.
    * @return {Promise<ClientResponse<void>>} A Promise for the FusionAuth call.
    */
    revokeRefreshToken: function(token){
        return this.client.revokeRefreshToken(token, "", "");
    },

    /**
    * Validates the provided JWT (encoded JWT string) to ensure the token is valid. A valid access token is properly
    * signed and not expired.
    * <p>
    * This API may be used to verify the JWT as well as decode the encoded JWT into human readable identity claims.
    *
    * @param {string} encodedJWT The encoded JWT (access token).
    * @return {Promise<ClientResponse<ValidateResponse>>} A Promise for the FusionAuth call.
    */
    validateJWT: function(encodedJWT){
        return this.client.validateJWT(encodedJWT);
    }
}

module.exports = HiveAuthClient;