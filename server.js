/* 
HIVE authentication client
server.js 
Created on: 2018/05/23
Author: Emilio Pena
(c) Qube 4D. All rights reserved
*/

const express = require('express');
const app =  express();
const bodyParser = require("body-parser");
var port = process.env.PORT || 3000;
const routes = require("./routes/routes");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(routes);

const server = app.listen(port,  (error) => {
    if (error) return console.log(`Error: ${error}`);
    
    console.log(`HIVE authentication client listening on port ${server.address().port}`);
});
