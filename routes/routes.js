/* 
 * routes.js 
 * Created on: 2018/05/23
 * Author: Emilio Pena 
 * Copyright (c) 2019, Qube 4D, All Rights Reserved
 */

const HiveAuthClient = require("../controller/hiveAuthClient");
const express = require("express");
const router = express.Router();

/**
 * Initializes a new HiveAuthClient
 * 
 * @param {HiveBaseRequest} req Request containing apiKey, hostURL, tenantId fields for initializing the HiveAuthClient. 
 * @return {HiveAuthClient}  A newly initialized HiveAuthClient object that can be used to query the FusionAuth server
 */
var initHiveClient = function(req){
    var apiKey = req.body.apiKey;
    var hostURL = req.body.hostURL;
    var tenantId = req.body.tenantId;

    var client = new HiveAuthClient(apiKey, hostURL, tenantId);
    return client;
}

/**   
 * Placeholder message for default endpoint
 * 
 * @return {string}  A placeholder message for  authentication API default endpoint.
 */
router.route("/").get((req, res) => {
    res.send({
        message: 'HIVE authentication client REST API. Copyright (c) 2019, Qube 4D, All Rights Reserved'
    });
});

/**   
 * User login operation, user enters username and password and receives an access token to be embedded in requests to the app server.
 * 
 * @param  {HiveLoginRequest} req Request containing loginId, password, applicationId for user login.
 * @return {LoginResponse}  An object containing the response from FusionAuth server.
 */
router.route("/api/login").post((req, res)=> {

    var hiveClient = initHiveClient(req);

    var loginId = req.body.loginId;
    var password = req.body.password;
    var applicationId = req.body.applicationId;

    hiveClient.login(loginId, password, applicationId).then((response) => {
        res.send(response.successResponse);
    }).catch((response) => {
        res.status(response.statusCode).send(response.errorResponse);
    });
});

/**   
 * User logout operation, user is logged out form the application corresponding to the passed refreshToken. 
 * Refresh token will continue to be valid after that.
 * 
 * @param {HiveLogoutRequest} req Request for user logout.  The global field specifies if the user is to be logged out from every app.
 * @return {void}  An object containing the response from FusionAuth server.
 */
router.route("/api/logout").post((req, res) => {

    var hiveClient = initHiveClient(req);

    var refreshToken = req.body.refreshToken;
    var global = false;

    if (Object.keys(req.body).includes("global")){
        global = req.body.global;
    }

    hiveClient.logout(global, refreshToken).then((response) => {
        res.send(response.successResponse);
    }).catch((response) => {
        res.status(response.statusCode).send(response.errorResponse);
    });
});

/**   
 * Issues a new JWT token for a second application to a logged user with a valid JWT access token.
 * 
 * @param {HiveIssueJWTRequest} req Request object to issue a JWT 
 * @return {IssueResponse}  An object containing the response from FusionAuth server
 */
router.route("/api/jwt/issue").post((req, res) => {

    var hiveClient = initHiveClient(req);

    var applicationId = req.body.applicationId;
    var encodedJWT = req.body.encodedJWT;

    hiveClient.issueJWT(applicationId, encodedJWT).then((response) => {
        res.send(response.successResponse);
    }).catch((response) => {
        res.status(response.statusCode).send(response.errorResponse);
    });
});

/**   
 * Issues a new JWT token in exchange of a valid refresh token.
 * 
 * @param {HiveRefreshJWTRequest} req Request to refreshing access token 
 * @return {RefreshResponse}  An object containing the response from FusionAuth server
 */
router.route("/api/jwt/refresh").post((req, res) => {

    var hiveClient = initHiveClient(req);

    var refreshToken = req.body.refreshToken;

    hiveClient.refreshAccessToken(refreshToken).then((response) =>{
        res.send(response.successResponse);
    }).catch((response) => {
        res.status(response.statusCode).send(errorResponse);
    })
});

/**   
 * Retrieves refresh tokens for a given user.
 * 
 * @param {HiveUserRefreshTokensRequest} req Request object to retrieve a user's refresh tokens
 * @return {RefreshResponse}  An object containing the response from FusionAuth server
 */
router.route("/api/jwt/retrieve-refresh-tokens").post((req, res) => {

    var hiveClient = initHiveClient(req);

    var userId = req.body.userId;

    hiveClient.retrieveUserRefreshTokens(userId).then((response)=> {
        res.send(response.successResponse);
    }).catch((response) => {
        res.status(response.statusCode).send(response.errorResponse);
    });
});

/**   
 * Revokes all refresh tokens for a given user.
 * 
 * @param {HiveUserRevokeRefreshTokensRequest} req Request object for revoking all refresh tokens belonging to a user
 * @return {void}  An object containing the response from FusionAuth server
 */
router.route("/api/jwt/revoke-user-refresh-tokens").post((req, res) => {

    var hiveClient = initHiveClient(req);

    var userId = req.body.userId;

    hiveClient.revokeUserRefreshTokens(userId).then((response)=> {
        res.send(response.successResponse);
    }).catch((response) => {
        res.status(response.statusCode).send(response.errorResponse);
    });
});

/**   
 * Revokes all refresh tokens for a given app.
 * 
 * @param {HiveAppRevokeRefreshTokensRequest} req Request object for revoking all refresh tokens belonging to an app.
 * @return {void}  An object containing the response from FusionAuth server
 */
router.route("/api/jwt/revoke-app-refresh-tokens").post((req, res) => {

    var hiveClient = initHiveClient(req);

    var applicationId = req.body.applicationId;

    hiveClient.revokeAppRefreshTokens(applicationId).then((response)=> {
        res.send(response.successResponse);
    }).catch((response) => {
        res.status(response.statusCode).send(response.errorResponse);
    });
});

/**   
 * Revokes all refresh tokens for a user in a given app.
 * 
 * @param {HiveUserAppRevokeRefreshTokensRequest} req Request object for revoking all refresh tokens belonging to an app.
 * @return {void}  An object containing the response from FusionAuth server
 */
router.route("/api/jwt/revoke-user-app-refresh-tokens").post((req, res) => {

    var hiveClient = initHiveClient(req);

    var userId = req.body.userId;
    var applicationId = req.body.applicationId;

    hiveClient.revokeUserAppRefreshTokens(userId, applicationId).then((response)=> {
        res.send(response.successResponse);
    }).catch((response) => {
        res.status(response.statusCode).send(response.errorResponse);
    });
});

/**   
 * Revokes the specified refresh token.
 * 
 * @param {HiveRevokeRefreshTokenRequest} req Request object for revoking the specified refresh token.
 * @return {void}  An object containing the response from FusionAuth server
 */
router.route("/api/jwt/revoke-refresh-token").post((req, res) => {

    var hiveClient = initHiveClient(req);

    var token = req.body.token;

    hiveClient.revokeRefreshToken(token).then((response)=> {
        res.send(response.successResponse);
    }).catch((response) => {
        res.status(response.statusCode).send(response.errorResponse);
    });
});

/**   
 * Validates an acccess token.
 * 
 * @param {HiveValidateTokenRequest} req Request object for validating an access token (encoded JWT).
 * @return {ValidateResponse}  An object containing the response from FusionAuth server.
 */
router.route("/api/jwt/validate").post((req, res) => {
    var hiveClient = initHiveClient(req);

    var encodedJWT = req.body.encodedJWT;

    hiveClient.validateJWT(encodedJWT).then((response) =>{
        res.send(response.successResponse);
    }).catch((response) => {
        res.status(response.statusCode).send(response.errorResponse);
    });
});

/**
 * HIVE base request object.
 *
 * @author Emilio Pena
 *
 * @typedef {Object} HiveBaseRequest
 *
 * @property {string} [apiKey]
 * @property {string} [hostURL]
 * @property {string} [tenantId]
 */

/**
 * HIVE login request object.
 *
 * @author Emilio Pena
 *
 * @typedef {Object} HiveLoginRequest
 * @extends HiveBaseRequest
 *
 * @property {string} [loginId]
 * @property {string} [password]
 * @property {string} [applicationId]
 */

/**
 * HIVE logout request object.
 *
 * @author Emilio Pena
 *
 * @typedef {Object} HiveLogoutRequest
 * @extends HiveBaseRequest
 *
 * @property {string} [refreshToken]
 * @property {boolean} [global]
 */

/**
 * HIVE issue JWT request object.
 *
 * @author Emilio Pena
 *
 * @typedef {Object} HiveIssueJWTRequest
 * @extends HiveBaseRequest
 *
 * @property {string} [applicationId]
 * @property {boolean} [encodedJWT]
 */

/**
 * HIVE Refresh JWT request object.
 *
 * @author Emilio Pena
 *
 * @typedef {Object} HiveRefreshJWTRequest
 * @extends HiveBaseRequest
 *
 * @property {string} [refreshToken]
 */

/**
 * HIVE user refresh tokens request object.
 *
 * @author Emilio Pena
 *
 * @typedef {Object} HiveUserRefreshTokensRequest
 * @extends HiveBaseRequest
 *
 * @property {string} [userId]
 */

/**
 * HIVE revoke user's refresh tokens request object.
 *
 * @author Emilio Pena
 *
 * @typedef {Object} HiveUserRevokeRefreshTokensRequest
 * @extends HiveBaseRequest
 *
 * @property {string} [userId]
 */

/**
 * HIVE revoke app's refresh tokens request object.
 *
 * @author Emilio Pena
 *
 * @typedef {Object} HiveAppRevokeRefreshTokensRequest
 * @extends HiveBaseRequest
 *
 * @property {string} [applicationId]
 */

/**
 * HIVE revoke user's refresh tokens in app request object.
 *
 * @author Emilio Pena
 *
 * @typedef {Object} HiveUserAppRevokeRefreshTokensRequest
 * @extends HiveUserRevokeRefreshTokensRequest
 *
 * @property {string} [applicationId]
 */

/**
 * HIVE revoke specified refresh token request object.
 *
 * @author Emilio Pena
 *
 * @typedef {Object} HiveRevokeRefreshTokenRequest
 * @extends HiveBaseRequest
 *
 * @property {string} [token]
 */

/**
 * HIVE validate access token request object.
 *
 * @author Emilio Pena
 *
 * @typedef {Object} HiveValidateTokenRequest
 * @extends HiveBaseRequest
 *
 * @property {string} [encodedJWT]
 */

module.exports = router;

